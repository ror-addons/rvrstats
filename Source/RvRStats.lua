RvRStats = {}

local window = "CharacterWindowRvRStats"
RvRStats.Window = window
local playername = WStringToString(GameData.Player.name)
local oldShow = nil

---- Init ----
function RvRStats.Init()
   oldShow = CharacterWindow.OnShown
   CharacterWindow.OnShown = RvRStats.OnShown
end

function RvRStats.OnShown()
   RvRStats.UpdateWindow()
   oldShow()
end

local function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end


function RvRStats.UpdateWindow()
   local SafeDeaths = math.max(GameData.Player.RvRStats.LifetimeDeaths, 1)
   local KDRatio = (GameData.Player.RvRStats.LifetimeKills / SafeDeaths)
   local DBDRatio = (GameData.Player.RvRStats.LifetimeDeathBlows / SafeDeaths)
   LabelSetText(window.."Header", GameData.Player.name)
   LabelSetText(window.."Title", L"RR: ".. GameData.Player.Renown.curRank .. L"   Title: " .. GameData.Player.Renown.curTitle)
   LabelSetText(window.."LifetimeK", L"Lifetime Kills: " .. (GameData.Player.RvRStats.LifetimeKills))
   LabelSetText(window.."LifetimeDB", L"Lifetime Deathblows: " .. (GameData.Player.RvRStats.LifetimeDeathBlows))
   LabelSetText(window.."LifetimeD", L"Lifetime Deaths: " .. (GameData.Player.RvRStats.LifetimeDeaths))
   LabelSetText(window.."LifetimeRatioKD", (StringToWString(string.format("Kill/Death Ratio: %.2f" , KDRatio))))
   LabelSetText(window.."LifetimeRatioDBD", (StringToWString(string.format("Deathblow/Death Ratio: %.2f" , DBDRatio))))
   LabelSetText(window.."SessionK", L"Session Kills: " .. (GameData.Player.RvRStats.SessionKills))
   --LabelSetText(window.."XPBonus", L"RvR XP Bonus: " .. (GameData.Player.RvRStats.XPBonusBuff))
   --LabelSetText(window.."RenownBonus", L"RvR RP Bonus: " .. (GameData.Player.RvRStats.RenownBonusBuff))

   LabelSetText(window.."ClassKills", L"Tome Recorded Kills")

   local race = GameData.Player.race.id
   local classA = 0
   local classB = 0
   local classC = 0
   local classD = 0
   local classE = 0
   local classF = 0
   local classG = 0
   local classH = 0
   local classI = 0
   local classJ = 0
   local classK = 0
   local classL = 0
   
   if race == 2 or race == 3 or race == 5 or race == 7 then
      local subTypeData = TomeGetBestiarySubTypeData( 16 ) --human
      if subTypeData then
         for index, speciesData in ipairs( subTypeData.species ) do
            if speciesData.id == 51 then classL = speciesData.killCount end --witch hunter
            if speciesData.id == 50 then classJ = speciesData.killCount end --warrior priest
            if speciesData.id == 48 then classB = speciesData.killCount end --bright wizard
            if speciesData.id == 49 then classE = speciesData.killCount end --kotbs
         end
      end
      local subTypeData = TomeGetBestiarySubTypeData( 13 ) --dwarf
      if subTypeData then
         for index, speciesData in ipairs( subTypeData.species ) do
            if speciesData.id == 44 then classD = speciesData.killCount end --ironbreaker
            if speciesData.id == 45 then classF = speciesData.killCount end --runepriest
            if speciesData.id == 42 then classC = speciesData.killCount end --engineer
            if speciesData.id == 43 then classH = speciesData.killCount end --slayer
         end
      end
      local subTypeData = TomeGetBestiarySubTypeData( 14 ) -- elf
      if subTypeData then
         for index, speciesData in ipairs( subTypeData.species ) do
            if speciesData.id == 78 then classI = speciesData.killCount end --swordmaster
            if speciesData.id == 76 then classK = speciesData.killCount end --white lion
            if speciesData.id == 75 then classA = speciesData.killCount end --archmage
            if speciesData.id == 77 then classG = speciesData.killCount end --shadow warrior
         end
      end
      LabelSetText(window.."ClassA", L"Archmage: " .. classA)
      LabelSetText(window.."ClassB", L"Bright Wizard: " .. classB)
      LabelSetText(window.."ClassC", L"Engineer: " .. classC)
      LabelSetText(window.."ClassD", L"Ironbreaker: " .. classD)
      LabelSetText(window.."ClassE", L"KotBS: " .. classE)
      LabelSetText(window.."ClassF", L"Runepriest: " .. classF)
      LabelSetText(window.."ClassG", L"Shadow Warrior: " .. classG)
      LabelSetText(window.."ClassH", L"Slayer: " .. classH)
      LabelSetText(window.."ClassI", L"Swordmaster: " .. classI)
      LabelSetText(window.."ClassJ", L"Warrior Priest: " .. classJ)
      LabelSetText(window.."ClassK", L"White Lion: " .. classK)
      LabelSetText(window.."ClassL", L"Witch Hunter: " .. classL) 
   end      
   if race == 1 or race == 4 or race == 6  then

      local subTypeData = TomeGetBestiarySubTypeData( 16 ) --human
      if subTypeData then
         for index, speciesData in ipairs( subTypeData.species ) do
            if speciesData.id == 14 then classD = speciesData.killCount end --chosen
            if speciesData.id == 16 then classG = speciesData.killCount end --marauder
            if speciesData.id == 17 then classL = speciesData.killCount end --zealot
            if speciesData.id == 15 then classF = speciesData.killCount end --magus
         end
      end
      local subTypeData = TomeGetBestiarySubTypeData( 15 ) --greenskin
      if subTypeData then
         for index, speciesData in ipairs( subTypeData.species ) do
            if speciesData.id == 63 then classH = speciesData.killCount end --shaman
            if speciesData.id == 64 then classJ = speciesData.killCount end --squig herder
            if speciesData.id == 92 then classB = speciesData.killCount end --black orc
            if speciesData.id == 93 then classC = speciesData.killCount end --choppa
         end
      end
      local subTypeData = TomeGetBestiarySubTypeData( 14 ) -- elf
      if subTypeData then
         for index, speciesData in ipairs( subTypeData.species ) do
            if speciesData.id == 31 then classK = speciesData.killCount end --witch elf
            if speciesData.id == 32 then classE = speciesData.killCount end --disciple of khaine
            if speciesData.id == 33 then classA = speciesData.killCount end --blackguard
            if speciesData.id == 34 then classI = speciesData.killCount end --sorcerer
         end
      end
      LabelSetText(window.."ClassA", L"Black Guard: " .. classA)
      LabelSetText(window.."ClassB", L"Black Orc: " .. classB)
      LabelSetText(window.."ClassC", L"Choppa: " .. classC)
      LabelSetText(window.."ClassD", L"Marauder: " .. classD)
      LabelSetText(window.."ClassE", L"Disciple of Khaine: " .. classE)
      LabelSetText(window.."ClassF", L"Magus: " .. classF)
      LabelSetText(window.."ClassG", L"Chosen: " .. classG)
      LabelSetText(window.."ClassH", L"Shaman: " .. classH)
      LabelSetText(window.."ClassI", L"Sorcerer: " .. classI)
      LabelSetText(window.."ClassJ", L"Squig Herder: " .. classJ)
      LabelSetText(window.."ClassK", L"Witch Elf: " .. classK)
      LabelSetText(window.."ClassL", L"Zealot: " .. classL)
   end      
   
end
