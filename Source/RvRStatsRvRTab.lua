--- Contains BulkTab Functions
RvRStatsRvRTab = {}

local oldHide = nil
local oldUpdate = nil
local oldShow = nil

CharacterWindow.MODE_RVR = 6

function RvRStatsRvRTab.Init()

   -- Bugs in open and close events, so hooking
   RvRStatsRvRTab.HookFuncs()
   
   -- Create Window
   CreateWindow("CharacterWindowTabRvRStats", false)
   CreateWindow("CharacterWindowRvRStats", false)

   -- Set Tab Anchors
   WindowClearAnchors("CharacterWindowTabRvRStats")
   WindowAddAnchor("CharacterWindowTabRvRStats", "topleft", "CharacterWindowTabsTimeoutTab", "topright", 0, 0)

   -- Set tab button text
   ButtonSetText("CharacterWindowTabRvRStats", L"RvR")
   
   -- Set parents 
   WindowSetParent( "CharacterWindowTabRvRStats", "CharacterWindowTabs" )
   WindowSetParent( "CharacterWindowRvRStats", "CharacterWindow" )
   
   -- Close CharacterWindow
   WindowSetShowing( "CharacterWindow", false )
end

---***********************************---
-- ********** Function Hooks ********* --
---***********************************---

function RvRStatsRvRTab.HookFuncs()
   oldUpdate = CharacterWindow.UpdateMode
   CharacterWindow.UpdateMode = RvRStatsRvRTab.UpdateMode

   oldShow = CharacterWindow.OnShown
   CharacterWindow.OnShown = RvRStatsRvRTab.Show
end

function RvRStatsRvRTab.Show()
   if ( CharacterWindow.mode ~= CharacterWindow.MODE_DYE_MERCHANT ) then
      WindowSetShowing( "CharacterWindowTabRvRStats", true )
   end
   oldShow()
end

function RvRStatsRvRTab.UpdateMode( mode )
   if( mode == CharacterWindow.MODE_RVR ) then
      oldUpdate(CharacterWindow.MODE_NORMAL)
      WindowSetShowing( "CharacterWindowContents", false )
      WindowSetShowing( "CharacterWindowBrags", false )
      WindowSetShowing( "CharacterWindowRvRStats", true )
      WindowSetShowing( "CharacterWindowTabs", true )
      ButtonSetPressedFlag( "CharacterWindowTabsCharTab", false )
      ButtonSetStayDownFlag( "CharacterWindowTabsCharTab", false )
      ButtonSetPressedFlag( "CharacterWindowTabsBragsTab", false )
      ButtonSetStayDownFlag( "CharacterWindowTabsBragsTab", false )
      ButtonSetPressedFlag( "CharacterWindowTabRvRStats", true )
      ButtonSetStayDownFlag( "CharacterWindowTabRvRStats", true )
   elseif( mode == CharacterWindow.MODE_DYE_MERCHANT ) then
      oldUpdate(mode)
      WindowSetShowing( "CharacterWindowRvRStats", false )
   else
      oldUpdate(mode)
      WindowSetShowing( "CharacterWindowRvRStats", false )
      ButtonSetPressedFlag( "CharacterWindowTabRvRStats", false )
      ButtonSetStayDownFlag( "CharacterWindowTabRvRStats", false )
   end
end

function RvRStatsRvRTab.OnTabSelectRvR()
    CharacterWindow.UpdateMode( CharacterWindow.MODE_RVR )
end








