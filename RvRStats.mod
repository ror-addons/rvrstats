<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <UiMod name="RvRStats" version="2.1" date="11/11/2010" >

    <Author name="Felyza" email="felyza@gmail.com" />
    <Description text="Adds RvR stats to the character window." />
	<VersionSettings gameVersion="1.4.1" windowsVersion="1.0" savedVariablesVersion="1.0" />
	
    <Dependencies>
      <Dependency name="EASystem_Utils" />
      <Dependency name="EA_ChatWindow"/>
      <Dependency name="EA_CharacterWindow"/>
      <Dependency name="LibSlash" />
    </Dependencies>

    <Files>
      <File name="Source/RvRStats.lua"/>
      <File name="Source/RvRStats.xml"/>
      <File name="Source/RvRStatsRvRTab.lua"/>
      <File name="Source/RvRStatsRvRTab.xml"/>
    </Files>

    <OnInitialize>
      <CallFunction name="RvRStatsRvRTab.Init" />
      <CallFunction name="RvRStats.Init" />
    </OnInitialize>
    <OnUpdate>
    </OnUpdate>
    <OnShutdown>
      <CallFunction name="RvRStats.OnShutDown" />
    </OnShutdown>
    <SavedVariables>
    </SavedVariables>
  </UiMod>
</ModuleFile>
